" Based on http://fugal.net/vim/colors/bw.vim

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors
"
hi clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name = 'eink'

set background=light
hi SpecialKey     cterm=bold        ctermfg=NONE
hi NonText        cterm=bold            ctermfg=NONE
hi Directory      cterm=bold            ctermfg=NONE
hi ErrorMsg       cterm=standout        ctermfg=White
hi IncSearch      cterm=reverse         ctermfg=NONE
hi Search         cterm=reverse         ctermfg=NONE
hi MoreMsg        cterm=bold            ctermfg=NONE
hi ModeMsg        cterm=bold            ctermfg=NONE
hi LineNr         cterm=bold       ctermfg=NONE
hi Question       cterm=standout        ctermfg=NONE
hi StatusLine     cterm=bold,reverse    ctermfg=NONE
hi StatusLineNC   cterm=reverse     ctermfg=NONE
hi VertSplit      cterm=reverse     ctermfg=NONE
hi Title          cterm=bold        ctermfg=NONE
hi Visual         cterm=reverse     ctermfg=NONE
hi VisualNOS      cterm=bold,underline  ctermfg=NONE
hi WarningMsg     cterm=standout    ctermfg=NONE
hi WildMenu       cterm=standout    ctermfg=NONE
hi Folded         cterm=standout    ctermfg=NONE
hi FoldColumn     cterm=standout    ctermfg=NONE
hi DiffAdd        cterm=bold            ctermfg=NONE
hi DiffChange     cterm=bold            ctermfg=NONE
hi DiffDelete     cterm=bold            ctermfg=NONE
hi DiffText       cterm=reverse         ctermfg=NONE
"hi Comment        cterm=underline          ctermfg=NONE
hi Constant       cterm=underline   ctermfg=NONE
hi Special        cterm=bold        ctermfg=NONE
hi Identifier     cterm=underline   ctermfg=NONE
hi Statement      cterm=bold        ctermfg=NONE
hi PreProc        cterm=bold   ctermfg=NONE
hi Type           cterm=bold   ctermfg=NONE
hi Underlined     cterm=underline   ctermfg=NONE
hi Ignore         cterm=bold        ctermfg=NONE
hi Error          cterm=standout     ctermfg=NONE
hi Todo           cterm=standout    ctermfg=NONE
hi SpellBad        cterm=reverse,underline   ctermBg=Grey
hi SpellCap        cterm=reverse,underline   ctermBg=Grey
hi YcmErrorSection cterm=reverse,underline   ctermBg=Grey

hi Pmenu ctermbg=Grey cterm=underline
"call s:hi('PmenuSel', s:sblue, s:white)

